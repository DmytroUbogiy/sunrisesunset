package com.home.android.sunrisesunset.model.network;

import com.home.android.sunrisesunset.model.Response;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public interface WebService {

    @GET("/json")
    Observable<Response> getSunriseSunsetForCity(@Query("lat") double lat,
                                                 @Query("lng") double lng,
                                                 @Query("date") String date);
}
