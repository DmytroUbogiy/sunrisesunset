package com.home.android.sunrisesunset.model.network;

import com.home.android.sunrisesunset.ui.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

@Singleton
@Component(modules = NetworkModule.class)
public interface NetworkComponent {

    void inject(MainActivity mainActivity);
}
