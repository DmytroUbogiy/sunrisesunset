package com.home.android.sunrisesunset.model;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public class SunriseSunset {

    public String sunrise;
    public String sunset;
}
