package com.home.android.sunrisesunset.utils.rx;

import io.reactivex.Scheduler;

/**
 * Created by dmytroubogiy on 23.12.17.
 */

public interface SchedulerProvider {

    Scheduler ui();
    Scheduler computation();
    Scheduler io();
}
