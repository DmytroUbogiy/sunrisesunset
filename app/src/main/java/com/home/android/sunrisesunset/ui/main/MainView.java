package com.home.android.sunrisesunset.ui.main;

import android.support.annotation.StringRes;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.home.android.sunrisesunset.model.City;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public interface MainView {

    void displaySunrise(String sunrise);
    void displaySunset(String sunset);
    void displayCity(City city);
    void displayError(String error);
    void showLoading();
    void hideLoading();
}
