package com.home.android.sunrisesunset;

import android.app.Application;

import com.home.android.sunrisesunset.model.network.DaggerNetworkComponent;
import com.home.android.sunrisesunset.model.network.NetworkComponent;
import com.home.android.sunrisesunset.model.network.NetworkModule;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public class SunriseSunsetApp extends Application {

    private NetworkComponent networkComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        networkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule())
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }
}
