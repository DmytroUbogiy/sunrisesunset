package com.home.android.sunrisesunset.ui.main;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.home.android.sunrisesunset.R;
import com.home.android.sunrisesunset.SunriseSunsetApp;
import com.home.android.sunrisesunset.model.City;
import com.home.android.sunrisesunset.model.network.WebService;
import com.home.android.sunrisesunset.utils.Utils;
import com.home.android.sunrisesunset.utils.rx.AppSchedulerProvider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.textViewSunset)
    TextView tvSunset;
    @BindView(R.id.textViewCityName)
    TextView tvCityName;
    PlaceAutocompleteFragment placeAutocompleteFragment;
    @BindView(R.id.textViewSunrise)
    TextView tvSunrise;
    ProgressDialog progressDialog;

    @Inject
    WebService webService;

    MainPresenter presenter;

    Unbinder unbinder;

    FusedLocationProviderClient fusedLocationProviderClient;

    private static final int FINE_LOCATION_CODE = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        ((SunriseSunsetApp) getApplication()).getNetworkComponent().inject(this);
        placeAutocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.placeAutocompleteFragment);
        AutocompleteFilter.Builder autoCompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES);
        placeAutocompleteFragment.setFilter(autoCompleteFilter.build());
        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                placeAutocompleteFragment.setText("");
                presenter.loadSunriseSunsetForCity(place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {

            }
        });
        presenter = new MainPresenterImpl(webService, new CompositeDisposable(), new AppSchedulerProvider());
        presenter.setView(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        checkLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case FINE_LOCATION_CODE:
                checkLocation();
                break;
        }
    }

    @Override
    public void displaySunrise(String sunrise) {
        tvSunrise.setText(String.format("Sunrise: %s", sunrise));
    }

    @Override
    public void displaySunset(String sunset) {
        tvSunset.setText(String.format("Sunset: %s", sunset));
    }

    @Override
    public void displayCity(City city) {
        String cityName = city.getName(new Geocoder(this));
        placeAutocompleteFragment.setText(cityName);
        tvCityName.setText(cityName);
    }

    @Override
    public void displayError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = Utils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        unbinder.unbind();
        super.onDestroy();
    }

    private void requestPermissions(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
    }

    private void checkLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(Manifest.permission.ACCESS_FINE_LOCATION, FINE_LOCATION_CODE);
        }
        else {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                presenter.loadSunriseSunsetForCity(location.getLatitude(), location.getLongitude());
                            }
                            presenter.loadSunriseSunsetForCity(0,0);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
            fusedLocationProviderClient.requestLocationUpdates(new LocationRequest(), new LocationCallback() {
                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    super.onLocationAvailability(locationAvailability);
                    if (locationAvailability.isLocationAvailable()) {
                        Toast.makeText(MainActivity.this, "Location available", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Location unavailable", Toast.LENGTH_SHORT).show();
                    }
                }
            }, null);
        }
    }
}
