package com.home.android.sunrisesunset.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public class Response {

   public SunriseSunset results;
   public String status;
}
