package com.home.android.sunrisesunset.ui.main;

import com.home.android.sunrisesunset.model.City;
import com.home.android.sunrisesunset.model.Response;
import com.home.android.sunrisesunset.model.SunriseSunset;
import com.home.android.sunrisesunset.model.network.WebService;
import com.home.android.sunrisesunset.utils.rx.SchedulerProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 Created by dmytroubogiy on 19.04.18.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView view;
    private WebService webService;
    private CompositeDisposable compositeDisposable;
    private SchedulerProvider schedulerProvider;

    MainPresenterImpl(WebService webService, CompositeDisposable compositeDisposable, SchedulerProvider schedulerProvider) {
        this.webService = webService;
        this.compositeDisposable = compositeDisposable;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void loadSunriseSunsetForCity(final double lat, final double lng) {
        view.showLoading();
        compositeDisposable.add(webService.getSunriseSunsetForCity(lat, lng, "today")
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(new Consumer<Response>() {
                    @Override
                    public void accept(Response response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        view.hideLoading();
                        view.displaySunrise(toLocalTime(response.results.sunrise));
                        view.displaySunset(toLocalTime(response.results.sunset));
                        createCity(lat, lng);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }
                        view.hideLoading();
                        view.displayError(throwable.getMessage());
                    }
                })
        );
    }

    private void createCity(double lat, double lng) {
        City city = new City();
        city.lat = lat;
        city.lng = lng;
        view.displayCity(city);
    }

    private boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void setView(MainView view) {
        this.view = view;
    }

    @Override
    public void destroy() {
        view = null;
    }

    private String toLocalTime(String time) {
        SimpleDateFormat df = new SimpleDateFormat("h:mm:ss a", Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(time);
            df.setTimeZone(TimeZone.getDefault());
            return df.format(date);
        } catch (ParseException e) {
            return time;
        }
    }
}
