package com.home.android.sunrisesunset.model;

import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public class City {

    public double lat;
    public double lng;
    private String name;

    public String getName(Geocoder geocoder) {
        String cityName = "Unknown";
        try {
            List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);
            cityName = addressList.get(0).getLocality();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cityName;
    }
}
