package com.home.android.sunrisesunset.ui.main;

import com.home.android.sunrisesunset.model.City;

/**
 * Created by dmytroubogiy on 19.04.18.
 */

public interface MainPresenter {

    void loadSunriseSunsetForCity(double lat, double lng);
    void setView(MainView view);
    void destroy();
}
